package com.janeto.jtc.bai7_luutru_du_lieu;

import android.app.Application;

public class ApplicationDemo extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferencesManager.init(this);
    }
}
