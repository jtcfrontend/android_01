package com.janeto.jtc.bai7_luutru_du_lieu;

import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (SharedPreferencesManager.isFirstTimeSetup()) {

            SharedPreferencesManager.setFirstTimeSetup(false);
        }
        readFile();
        getPublicAlbumStorageDir("Camera");
    }

    private void readFile() {
        File fileDir = getFilesDir();
//        Log.d("file dir", fileDir.getAbsolutePath());
//        File newFile = new File(fileDir,"newfile");
//        FileOutputStream outputStream;
//        String fileContents = "Hello world!";
//        try {
//            outputStream = openFileOutput("new file", Context.MODE_PRIVATE);
//            outputStream.write(fileContents.getBytes());
//            outputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        FileInputStream newfile = openFileInput(fileDir.getAbsolutePath() + "new file");
        Log.d("file", openFile("new file"));
    }

    private String openFile(String filename){
        FileInputStream fis;
        try {
            fis = openFileInput(filename);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }

    public File getPublicAlbumStorageDir(String albumName) {
// Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("public", "Directory not created"); }
        return file;
    }
}
