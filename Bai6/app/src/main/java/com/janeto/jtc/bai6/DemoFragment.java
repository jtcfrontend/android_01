package com.janeto.jtc.bai6;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class DemoFragment extends Fragment {
    private EditText userName;
    private EditText password;
    private Button submit;
    private onFragmentDemo onFragmentDemo;



    public DemoFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_demo, container, false);
        initView(view);
        return view;

    }
    private void initView(View view){
        userName = view.findViewById(R.id.login_edt_username);
        password = view.findViewById(R.id.login_edt_password);
        submit = view.findViewById(R.id.login_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onFragmentDemo !=null) {
                    onFragmentDemo.onLoginSuccess();
                }
            }
        });

    }

    public void setOnFragmentDemo(com.janeto.jtc.bai6.onFragmentDemo onFragmentDemo) {
        this.onFragmentDemo = onFragmentDemo;
    }
}
