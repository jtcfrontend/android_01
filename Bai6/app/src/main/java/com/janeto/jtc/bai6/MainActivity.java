package com.janeto.jtc.bai6;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button showLogin = findViewById(R.id.showLogin);
        showLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new DemoFragment();
                ((DemoFragment) fragment).setOnFragmentDemo(new onFragmentDemo() {
                    @Override
                    public void onLoginSuccess() {
                        Toast.makeText(MainActivity.this,"success",Toast.LENGTH_LONG).show();
                    }
                });
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_content, fragment, fragment.getClass().getSimpleName()).commit()
            }
        });
    }

}
