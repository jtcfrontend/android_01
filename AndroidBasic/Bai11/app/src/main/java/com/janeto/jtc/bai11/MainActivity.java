package com.janeto.jtc.bai11;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private TextView text;
    private Button mPlayBtn;
    private Button mStopBtn;
    private WeatherService weatherService;
    private boolean binded = false;
    private TextView weatherText;
    private EditText locationText;
    private Button showWeather;
    private ServiceConnection weatherServiceConnection = new ServiceConnection() {


        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            WeatherService.LocalWeatherBinder binder = (WeatherService.LocalWeatherBinder) iBinder;
            weatherService = binder.getService();
            binded = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            binded = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.hello);
        mPlayBtn = findViewById(R.id.play);
        mStopBtn = findViewById(R.id.stop);
        mPlayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playSong();
            }
        });
        mStopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopSong();
            }
        });

        weatherText = findViewById(R.id.weatherText);
        locationText = findViewById(R.id.locationEdittext);
        showWeather = findViewById(R.id.showWeather);

//        new Thread() {
//            public void run() {
//                try {
//                    Thread.sleep(5000);
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            ((TextView) findViewById(R.id.hello)).setText("abc");
//                        }
//                    });
////                    ((TextView) findViewById(R.id.hello)).setText("abc");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }.start();
//
//        Log.d("AAA","1");
//        Log.d("AAA","2");
//        new DownloadFilesTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        // Method này được gọi khi người dùng Click vào nút Start.

        showWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWeather();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
// Tạo đối tượng Intent cho WeatherService.
        Intent intent = new Intent(this, WeatherService.class);
// Gọi method bindService(..) để ràng buộc dịch vụ với giao diện.
        this.bindService(intent, weatherServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    // Khi Activity ngừng hoạt động.
    @Override
    protected void onStop() {
        super.onStop();
        if (binded) {
// Hủy ràng buộc kết nối với dịch vụ.
            this.unbindService(weatherServiceConnection);
            binded = false;
        }
    }

    // Khi click vào Button xem thời tiết.
    private void showWeather() {
        String location = locationText.getText().toString();
        String weather = this.weatherService.getWeatherToday(location);
        weatherText.setText(weather);
    }

    private void playSong() {
        // Tạo ra một đối tượng Intent cho một dịch vụ (PlaySongService).
        Intent myIntent = new Intent(MainActivity.this,
                PlaySongService.class);
        // Gọi phương thức startService (Truyền vào đối tượng Intent)
        this.startService(myIntent);
    }

    // Method này được gọi khi người dùng Click vào nút Stop.
    private void stopSong() {
        // Tạo ra một đối tượng Intent.
        Intent myIntent = new Intent(MainActivity.this,
                PlaySongService.class);
        this.stopService(myIntent);
    }

    class DownloadFilesTask extends AsyncTask<String, String, Long> {
        @Override
        protected void onProgressUpdate(String... values) {
//            super.onProgressUpdate(values);
            Log.d("haha", values[0] + "");
            text.setText(values[0] + "");
        }

        protected Long doInBackground(String... urls) {
            int count = urls.length;
            long totalSize = 0;
            for (int i = 0; i < count; i++) {
//                totalSize += Downloader.downloadFile(urls[i]);
                final int a = i;
                totalSize += a;
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress("haha" + a); // Escape early if cancel() is called
                if (isCancelled()) break;
            }
//            }
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            return totalSize;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            text.setText(aLong + "");
            Log.d("haha", aLong + "");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}