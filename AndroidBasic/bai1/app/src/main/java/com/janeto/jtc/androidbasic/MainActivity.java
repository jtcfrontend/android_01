package com.janeto.jtc.androidbasic.bai1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.janeto.jtc.androidbasic.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button mGoBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        Log.d("MainActivity","onCreate");
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity","onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MainActivity","onRestart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity","onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity","onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity","onDestroy");
    }

    private void initView() {
        mGoBtn = findViewById(R.id.buttonGo);
        mGoBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.buttonGo) {
            onGoBtnClicked();
        }
    }

    private void onGoBtnClicked() {
        Intent intent = new Intent(this, com.janeto.jtc.androidbasic.bai1.Main2Activity.class);
        startActivity(intent);
    }
}
