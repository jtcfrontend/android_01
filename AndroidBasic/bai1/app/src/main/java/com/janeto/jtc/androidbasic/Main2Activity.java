package com.janeto.jtc.androidbasic.bai1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.janeto.jtc.androidbasic.R;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Log.d("Main2Activity","onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Main2Activity","onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Main2Activity","onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Main2Activity","onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Main2Activity","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Main2Activity","onDestroy");
    }
}
