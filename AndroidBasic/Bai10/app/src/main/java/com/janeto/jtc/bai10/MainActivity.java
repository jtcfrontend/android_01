package com.janeto.jtc.bai10;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private SeekBar seekBar;
    private Button rewindBtn;
    private Button fastForwardBtn;
    private Button startBtn;
    private Button stopBtn;
    private TextView maxTimeTv;
    private TextView currentTimeTv;
    private MediaPlayer mMediaPlayer;
    private Handler threadHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        int songId = this.getRawResIdByName("that_girl");
        mMediaPlayer = MediaPlayer.create(this, songId);
        threadHandler = new Handler();
    }

    private void initView() {
        seekBar = findViewById(R.id.seekBar);
        rewindBtn = findViewById(R.id.rewind);
        fastForwardBtn = findViewById(R.id.fastForward);
        startBtn = findViewById(R.id.start);
        stopBtn = findViewById(R.id.stop);
        maxTimeTv = findViewById(R.id.maxTimeTextView);
        currentTimeTv = findViewById(R.id.currentTimeTextView);
        seekBar.setClickable(false);
        startBtn.setOnClickListener(this);
        stopBtn.setOnClickListener(this);
        rewindBtn.setOnClickListener(this);
        fastForwardBtn.setOnClickListener(this);


    }

    private String millisecondsToString(int milliseconds) {
        long minutes = TimeUnit.MILLISECONDS.toMinutes((long) milliseconds);
        long seconds = TimeUnit.MILLISECONDS.toSeconds((long) milliseconds);
        return minutes + ":" + seconds;
    }

    public int getRawResIdByName(String resName) {
        String pkgName = this.getPackageName();
        return this.getResources().getIdentifier(resName, "raw", pkgName);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rewind:
                onRewindBtnClicked();
                break;
            case R.id.fastForward:
                onFastForwardBtnClicked();
                break;
            case R.id.start:
                onStartBtnClicked();
                break;
            case R.id.stop:
                onStopBtnClicked();
                break;
        }
    }

    private void onRewindBtnClicked() {
        int currentPosition = mMediaPlayer.getCurrentPosition();
        int duration = mMediaPlayer.getDuration();
// 5 giây.
        int SUBTRACT_TIME = 5000;
        if (currentPosition - SUBTRACT_TIME > 0) {
            this.mMediaPlayer.seekTo(currentPosition - SUBTRACT_TIME);
        }
    }

    private void onFastForwardBtnClicked() {
        int currentPosition = this.mMediaPlayer.getCurrentPosition();
        int duration = this.mMediaPlayer.getDuration();
// 5 giây.
        int ADD_TIME = 5000;
        if (currentPosition + ADD_TIME < duration) {
            this.mMediaPlayer.seekTo(currentPosition + ADD_TIME);

        }
    }

    private void onStartBtnClicked() {
        int duration = this.mMediaPlayer.getDuration();
        int currentPosition = this.mMediaPlayer.getCurrentPosition();
        if (currentPosition == 0) {
            seekBar.setMax(duration);
            String maxTimeString = this.millisecondsToString(duration);
            this.maxTimeTv.setText(maxTimeString);
        } else {
//            mMediaPlayer.reset();
        }

        mMediaPlayer.start();

        UpdateSeekBarThread updateSeekBarThread = new UpdateSeekBarThread();
        threadHandler.postDelayed(updateSeekBarThread, 50);
        this.stopBtn.setEnabled(true);
        this.startBtn.setEnabled(false);
    }

    private void onStopBtnClicked() {
        this.mMediaPlayer.pause();
        this.stopBtn.setEnabled(false);
        this.startBtn.setEnabled(true);
    }

    // Thread sử dụng để Update trạng thái cho SeekBar.
    class UpdateSeekBarThread implements Runnable {
        public void run() {
            int currentPosition = mMediaPlayer.getCurrentPosition();
            String currentPositionStr = millisecondsToString(currentPosition);
            currentTimeTv.setText(currentPositionStr);
            seekBar.setProgress(currentPosition); // Ngừng thread 50 mili giây.
            threadHandler.postDelayed(this, 50);
        }
    }
}
