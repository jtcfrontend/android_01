package com.janeto.jtc.bai2;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

public class GridViewExample extends AppCompatActivity {
private GridView gridView;
private Button addBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_example);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        initView();

    }
    private void initView(){
        gridView = findViewById(R.id.gridView1);
        addBtn = findViewById(R.id.buttonAdd);
        final List<Student> studentList = new ArrayList<>(0);
        for(int i = 0; i< 100; i++){
            studentList.add(new Student("student" + i,i + "",18));
        }
        final CustomAdapter customAdapter = new CustomAdapter(this,0,studentList,this.getLayoutInflater());
        gridView.setAdapter(customAdapter);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                studentList.add(new Student("new student","15",19));
                customAdapter.notifyDataSetChanged();
            }
        });
    }

}
