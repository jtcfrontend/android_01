package com.janeto.jtc.bai2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<Student> {
    private List<Student> data;
    private LayoutInflater inflater;
    public CustomAdapter(@NonNull Context context, int resource, @NonNull List<Student> objects, LayoutInflater inflater) {
        super(context, resource, objects);
        this.data = objects;
        this.inflater = inflater;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.custom_list_view_item,null);
            holder = new ViewHolder();
            holder.setTextViewName((TextView) convertView.findViewById(R.id.txtName));
            holder.setTextViewId((TextView) convertView.findViewById(R.id.txtId));
            holder.setTextViewAge((TextView) convertView.findViewById(R.id.txtAge));
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Student student = data.get(position);
        TextView textViewName = holder.getTextViewName();
        TextView textViewId = holder.getTextViewId();
        TextView textViewAge = holder.getTextViewAge();
        textViewName.setText(student.getName());
        textViewId.setText(student.getId());
        textViewAge.setText(student.getAge() + "");
        return convertView;
    }

    private static class ViewHolder{
        private TextView textViewName;
        private TextView textViewId;
        private TextView textViewAge;

        public TextView getTextViewName() {
            return textViewName;
        }

        public void setTextViewName(TextView textViewName) {
            this.textViewName = textViewName;
        }

        public TextView getTextViewId() {
            return textViewId;
        }

        public void setTextViewId(TextView textViewId) {
            this.textViewId = textViewId;
        }

        public TextView getTextViewAge() {
            return textViewAge;
        }

        public void setTextViewAge(TextView textViewAge) {
            this.textViewAge = textViewAge;
        }
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Nullable
    @Override
    public Student getItem(int position) {
        return data.get(position);

    }
}
