package com.janeto.jtc.bai2;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class DemoView extends View {
    private Paint paint;

    public DemoView(Context context) {
        super(context);
    }

    public DemoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
         paint = new Paint();
//        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.DemoViewAttrs);
//        float radiusDimen =  attributes.getDimension(R.styleable.DemoViewAttrs_radius_dimen,0);
////        Log.d()

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Rect bounds = new Rect();
        bounds.set(50, 0,700,200);
        paint.setColor(Color.BLUE);
        canvas.drawRect(bounds,paint);
        paint.setColor(Color.RED);
        canvas.drawCircle(200,200,100,paint);
        canvas.drawCircle(500,200,100,paint);
    }

}
