package main;

public class ConNguoi{
	private String ten;
	private int age;
	public ConNguoi() {
		
	}
	public ConNguoi(String ten, int age) {
		this.ten = ten;
		this.age = age;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
}