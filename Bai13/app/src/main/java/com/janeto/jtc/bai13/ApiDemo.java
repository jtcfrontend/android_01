package com.janeto.jtc.bai13;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface  ApiDemo {
    @GET("/")
    Call<Example> getGithub();
    @GET
    Call<Repos> getRepos(@Url String url);


}
