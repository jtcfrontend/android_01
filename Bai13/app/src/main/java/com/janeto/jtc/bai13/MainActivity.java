package com.janeto.jtc.bai13;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private TextView result;
    private TextView result2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button callBtn = findViewById(R.id.call);
        Button reposBtn = findViewById(R.id.repos);
        result = findViewById(R.id.textView);
        result2 = findViewById(R.id.textView2);
        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                callApi();
                callApiViaLib();
            }
        });

        reposBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiService.getInstance().getRepos("Trinhleo", "vegetableApp", new Callback<Repos>() {
                    @Override
                    public void onResponse(Call<Repos> call, Response<Repos> response) {
                        if(response.isSuccessful()) {
                            Repos repos = response.body();
                            result2.setText(repos.getCommentsUrl());
                        }
                    }

                    @Override
                    public void onFailure(Call<Repos> call, Throwable t) {

                    }
                });
            }
        });
    }

    private void callApi() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //Create URL
                try {
                    URL githubEndpoint = new URL("https://api.github.com/");
                    // Create connection
                    HttpsURLConnection myConnection = (HttpsURLConnection) githubEndpoint.openConnection();

                    myConnection.setRequestProperty("User-Agent", "my-rest-app-v0.1");

                    myConnection.setRequestProperty("Accept", "application/vnd.github.v3+json");

                    myConnection.setRequestProperty("Contact-Me",
                            "hathibelagal@example.com");

                    if (myConnection.getResponseCode() == 200) { // Success
                        InputStream responseBody = myConnection.getInputStream();

                        InputStreamReader responseBodyReader =
                                new InputStreamReader(responseBody, "UTF-8");

                        JsonReader jsonReader = new JsonReader(responseBodyReader);
                        Log.d("result", responseBody.toString());
                        jsonReader.beginObject();
                        while (jsonReader.hasNext()) {
                            String key = jsonReader.nextName();
//                            if (key.equals("notifications_url")) {

                                String value = jsonReader.nextString();

                                Log.d("result", value);

//                                break;
//                            } else {
//                                jsonReader.skipValue();
//                            }

                        }
                        jsonReader.close();
                        myConnection.disconnect();
                    } else {
// Error handling code goes here
Log.d("result", myConnection.getResponseCode() + "");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void callApiViaLib() {
        ApiService.getInstance().getGithub(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                if(response.isSuccessful()) {
                    Example example = response.body();
                    result.setText(example.getIssuesUrl());
                } else {

                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {

            }
        });
    }
}
